// before

let Cat = function (name, age) {
  this.name = name;
  this.age = age;
};
let tom = new Cat("Tom", 2);
let miu = new Cat("Miu", 2);
console.log("miu: ", miu);

console.log("tom: ", tom);

// after

class Dog {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
  sayHello() {
    console.log("hello");
  }
}
let bull = new Dog("Bull", 1.5);
console.log("bull: ", bull);
bull.sayHello();
