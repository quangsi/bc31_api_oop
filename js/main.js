// https://62b07874e460b79df0469ac8.mockapi.io/

import { monAnService, username } from "./service/monAnService.js";

import soLuong from "./service/monAnService.js";
import { spinnerService } from "./service/spinnerService.js";
import { monAnController } from "./controller/monAncontroller.js";
// let name = sv.username;
// let age = sv.username;
// let { username, age } = sv;

let foodList = [];

let idFoodEdited = null;
//  function xoá món ăn

let xoaMonAn = (maMonAn) => {
  spinnerService.batLoading();
  monAnService
    .xoaMonAn(maMonAn)
    .then((res) => {
      spinnerService.tatLoading();

      // sau khi xoá món ăn thành công
      renderDanhSachService();
    })
    .catch((err) => {
      spinnerService.tatLoading();
    });
};
window.xoaMonAn = xoaMonAn;

let layChiTietMonAn = (idMonAn) => {
  // gán id của món ăn dc chọn vào biến idFoodEdited
  idFoodEdited = idMonAn;
  spinnerService.batLoading();
  monAnService
    .layThongTinChiTietMonAn(idMonAn)
    .then((res) => {
      spinnerService.tatLoading();
      // lấy dữ liệu show lên giao diện ( binding dữ liệu);
      monAnController.showThongTinLenForm(res.data);
    })
    .catch((err) => {
      spinnerService.tatLoading();
    });
};

window.layChiTietMonAn = layChiTietMonAn;

let renderTable = (list) => {
  let contentHTML = "";
  for (let index = 0; index < list.length; index++) {
    let monAn = list[index];
    let contentTr = `<tr> 
                         <td> ${monAn.id} </td>
                         <td> ${monAn.name} </td>
                         <td> ${monAn.price} </td>
                         <td> ${monAn.description} </td>

                         <td>    
                                  <button
                                  onclick="layChiTietMonAn(${monAn.id})"
                                  class="btn btn-primary">Sửa</button>
                                  <button
                                  onclick="xoaMonAn(${monAn.id})"
                                  class="btn btn-warning">Xoá</button>
                         </td>
                     </tr>`;
    contentHTML = contentHTML + contentTr;
  }
  document.getElementById("tbody_food").innerHTML = contentHTML;
  // render danh sách món ăn ra ngoài màn hình
};
let renderDanhSachService = () => {
  spinnerService.batLoading();
  monAnService
    .layDanhSachMonAn()
    .then((res) => {
      spinnerService.tatLoading();

      foodList = res.data;

      renderTable(foodList);
    })
    .catch((err) => {
      spinnerService.tatLoading();
    });

  // dc 1
  // ko dc 0
};
// chạy lần đầu khi load lại trang
renderDanhSachService();

// thêm mới món ăn

let themMonAn = () => {
  // sử dụng monAnController
  let monAn = monAnController.layThongTinTuForm();

  spinnerService.batLoading();
  monAnService
    .themMoiMonAn(monAn)
    .then((res) => {
      spinnerService.tatLoading();
      renderDanhSachService();
    })
    .catch((err) => {
      spinnerService.tatLoading();

      alert("thất bại");
    });
};

window.themMonAn = themMonAn;

let capNhatMonAn = () => {
  console.log("yes");
  let monAn = monAnController.layThongTinTuForm();
  console.log("monAn: ", monAn);
  spinnerService.batLoading();

  let newMonAn = { ...monAn, id: idFoodEdited };
  console.log("newMonAn: ", newMonAn);

  monAnService
    .capNhatMonAn(newMonAn)
    .then((res) => {
      spinnerService.tatLoading();
      // console.log(res);
      // clear thông tin sau khi update thành công
      monAnController.showThongTinLenForm({
        name: "",
        price: "",
        description: "",
      });
      renderDanhSachService();
    })
    .catch((err) => {
      spinnerService.tatLoading();
      console.log(err);
    });
};

window.capNhatMonAn = capNhatMonAn;
// setTimeout(() => {
// }, 1000);
// setTimeout(() => {
// }, 100);
