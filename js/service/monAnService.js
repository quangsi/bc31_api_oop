const BASE_URL = "https://62b07874e460b79df0469ac8.mockapi.io/mon-an";
export let monAnService = {
  layDanhSachMonAn: () => {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },
  xoaMonAn: (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    });
  },
  themMoiMonAn: (monAn) => {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: monAn,
    });
  },
  layThongTinChiTietMonAn: (idMonAn) => {
    return axios({
      url: `${BASE_URL}/${idMonAn}`,
      method: "GET",
    });
  },
  capNhatMonAn: (monAn) => {
    return axios({
      url: `${BASE_URL}/${monAn.id}`,
      method: "PUT",
      data: monAn,
    });
  },
};

export let username = "Alice";

let number = 10;
// default : 1 lần duy nhất trong 1 file
export default number;
