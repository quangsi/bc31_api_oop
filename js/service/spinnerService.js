export const spinnerService = {
  batLoading: () => {
    // hiện loading
    document.getElementById("loading").style.display = "flex";
    // ẩn table
    document.getElementById("content").style.display = "none";
  },

  tatLoading: () => {
    document.getElementById("loading").style.display = "none";

    // bật table
    document.getElementById("content").style.display = "block";
  },
};
